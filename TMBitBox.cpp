//******************************************************************************
// TMBitBox.cpp
// Version 1.0.0 Dec, 2017
// Copyright 2017 Tevis Money
//
// This is a utility library for a simple bit box appliance. Useful for
// passing binary data in to a set of pins on the arduino.
// See .h file for documentation of public functions
//******************************************************************************

 #include "Arduino.h"
 #include "TMBitBox.h"


byte arraySize = 0; //Size of the inputPins array

TMBitBox::TMBitBox(){

}

TMBitBox::TMBitBox(uint8_t triggerPin, uint8_t inputPins[], uint8_t numPins) {
	start(triggerPin, inputPins, numPins);
}

void TMBitBox::start(uint8_t triggerPin, uint8_t inputPins[], uint8_t numPins) {
	myTriggerPin = triggerPin;
	myPins = inputPins;
	arraySize = numPins;

	pinMode(triggerPin, INPUT_PULLUP);
	for(int cv = 0; cv < arraySize; cv++){
		pinMode(myPins[cv], INPUT_PULLUP);
	}
}

bool TMBitBox::readRequested() {
	return digitalRead(myTriggerPin) == LOW;
}

uint8_t TMBitBox::read() {
	startDebug();
	uint8_t readResults[2];
	readByteOffset(readResults, 0);
	uint8_t value = readResults[0];
	endDebug();
	return  value;	
}

uint16_t TMBitBox::read16(){
	startDebug();
	uint16_t value = 0;
	int offset = 0;
	do {
		uint8_t readResults[2];
		readByteOffset(readResults, offset);
		if(readResults[1] > 0){
			value <<= readResults[1];
			value |= readResults[0];
		} else {
			break;
		}
		offset += 8;
	} while (offset <= 8);
	endDebug();
	return value;
}

uint32_t TMBitBox::read32(){
	startDebug();
	uint32_t value = 0;
	int offset = 0;
	do {
		uint8_t readResults[2];
		readByteOffset(readResults, offset);
		if(readResults[1] > 0){
			value <<= readResults[1];
			value |= readResults[0];
		} else {
			break;
		}
		offset += 8;
	} while (offset<=24);
	endDebug();
	return value;
}

//----------------Private Functions---------------------------------//

/**
 * Blink the LED_BUILTIN at a given delay frequency
 * Uses a trailing delay
 * @param the number of miliseconds to hold the LED on
 *		and off
 */
void TMBitBox::blinkLED(byte ms) {
	digitalWrite(LED_BUILTIN, HIGH);
	delay(ms);
	digitalWrite(LED_BUILTIN, LOW);
	delay(ms);
}

/**
 * Read one byte worth of pins from the input pin array
 * @param a pointer to an array to store the results in
 * @param offset the offset in the array to start reading 
 *		from, defaults to 0
 */
void TMBitBox::readByteOffset(uint8_t *results, int offset = 0){
	int limit = min(offset + 8, arraySize);
	uint8_t value = 0;
	results[1] = (uint8_t) (limit-offset);
	for(offset; offset < limit; offset++){
		value <<= 1;
		value |= digitalRead(myPins[offset]);
	}
	results[0] = value;
	return *results;
}

/**
 * Common start routine for all read functions
 */
void TMBitBox::startDebug(){
	while(digitalRead(myTriggerPin) == LOW){
		//Allow the user to release the trigger pin 
		// before attempting to read
		blinkLED(250);
	}

	while(digitalRead(myTriggerPin) == HIGH) {
		//Wait for the user to set the pins and
		// press the trigger button
		blinkLED(250);
	}

	digitalWrite(LED_BUILTIN, HIGH);
}

/**
 * Common end routine fr all read functions
 */
void TMBitBox::endDebug(){
	while(digitalRead(myTriggerPin) == LOW){
		//Wait for the user to release the trigger
		blinkLED(50);
	}

	for (byte cv = 0; cv < 3; cv++){
		blinkLED(100);
	}
}

