//******************************************************************************
// TMBitBox.h
// Version 1.0.0 Dec, 2017
// Copyright 2017 Tevis Money
//
// This is a utility library for a simple bit box appliance. Useful for
// passing binary data in to a set of pins on the arduino.
//******************************************************************************

#ifndef TMBitBox_h
#define TMBitBox_h

//------------------------------------------------------------------------------
// Include the right Arduino header
//
#if defined(ARDUINO) && (ARDUINO >= 100)
#	include <Arduino.h>
#else
#	if !defined(IRPRONTO)
#		include <WProgram.h>
#	endif
#endif

class TMBitBox {

public:

	/**
	 * Create a new BitBox object
	 */
	TMBitBox();

	/**
	 * Create a new BitBox object and immediately call
	 * start(uint8_t, uint8_t) with the provided values
	 */
	TMBitBox(uint8_t triggerPin, uint8_t inputPins[], uint8_t numPins);

	/**
	 * Start this instance of the BitBox
	 * @param triggerPin the pin on the arduino that the
	 *		BitBox will send LOW when wanting to enter
	 *		debug mode as well as read inputs
	 * @param inputPins[] an array of pins to read, arranged
	 *		from most to least significant bit. The code will
	 *		set the mode of these pins to INPUT_PULLUP.
	 * @param numPins the total number of pins in the array.
	 *		Because Arduino doesn't have standard vectors or linked
	 *		lists that know their own size.
	 */
	void start(uint8_t triggerPin, uint8_t inputPins[], uint8_t numPins);

	/*
	 * Read values from the defined input pins, in order of the
	 * provided inputPins array and return the value.
	 *
	 *   When read() is called, the function will blink LED_BUILTIN in 250ms cycles, 
	 * until the trigger pin transitions from HIGH to LOW, this 
	 * lets the user know they can begin setting the pins to be read.
	 * The LED_BUILTIN will continue to blink until the user sets the 
	 * trigger pin LOW again. 
	 *   Once the function detects that the trigger pin has gone low
	 * (may take up to ~500ms), the LED_BUILTIN will be set steady HIGH
	 * and the pins will be read. The LED_BUILTIN will then blink rapidly
	 * in 50 ms cycles until the trigger pin is set HIGH again, indicating 
	 * to the user the pins have been read.
	 *   Finally, once the trigger pin has gone HIGH, the LED_BUILTIN will
	 * blink 3 times indicating the values are being returned to the calling
	 * function.
	 *
	 * @return the value of the pins as a single byte, if less than
	 *		8 pins were defined, only the lower n bits will be set
	 *		if more than 8 pins were defined, only the first 8 will
	 *      be read (starting from the most significant bit).
	 */
	uint8_t read();

	/*
	 * As the read() function above, but returning 2 bytes
	 * @return the value of the pins
	 */
	uint16_t read16();

	/*
	 * As the read() function above, but returning 4 bytes
	 * @return the value of the pins
	 */
	uint32_t read32();

	/*
	 * Returns whether or not the trigger pin is LOW
	 * indicating that the user wants to enter debug mode
	 * @return true if the trigger pin is LOW
	 */
	bool readRequested();

private:
	byte myTriggerPin;
	uint8_t* myPins;
	void blinkLED(byte ms);
	void readByteOffset(uint8_t *results, int offset);
	void startDebug();
	void endDebug();
};


#endif