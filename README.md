A simple library for using a "bit box" appliance with an arduino to send binary data.


```c++
/**
 * Code Sample
 * Read data from analog pins 0-3 and 
 * use pin 4 as the trigger
 */
 
 TMBitBox bitbox();
 uint8_t pins[] = {A0, A1, A2, A3}
 bitbox.start(A4, pins, 4);
 
 if(bitbox.readRequested()){
    //The user is holding the trigger pin low
    
    uint8_t myValue = bitbox.read();
 }
 
```

See the files in the fritzing-files directory for a sample circuit for building a BitBox to use with this code. The bitbox in the sample could be made simpler by eliminating the dip switches and simply connecting or disconnecting data lines to a common ground. It is even possible (though unwieldy in use) to build the whole box with nothing more than a bread board and jumper wire, with no other components. The sample circuit envisions a simple but "ideal" version of the box that eliminates the need for connecting and disconnecting wires from the arduino to change data.

A BitBox requires a minimum of 2 pins on the arduino, one for the trigger button, and one data bin (if you want to send data 1 bit at a time). The code will handle a maximum of 32 data pins at once if you have the IO pins to spare. Additionally you will need a ground connection.

The sample above uses 5 pins (A4 as the trigger pin, and A0-A3 as the data pins) and a ground connection.
 
